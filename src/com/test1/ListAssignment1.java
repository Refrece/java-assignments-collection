package com.test1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class ListAssignment1 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Karthi");
		list.add("Sarath");
		list.add("Arun");
		System.out.println(list);
		HashSet<String> set = new HashSet(list);
		set.add("Mathi");
		Iterator<String> i = set.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}
	}
}
